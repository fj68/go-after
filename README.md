# After - Simple CLI Timer

```sh
after 3s && echo 'boom!'
```

## Usage

```sh
after [options] <duration>
```

This command blocks until `<duration>` passes and return `0`.
Press `Ctrl-C` to quit.

`<duration>` is a possibly signed sequence of decimal numbers,
each with optional fraction and a unit suffix such as:
	`300ms`, `2h45m` or `1h10m10s`

Valid time units are:
	`ms`, `s`, `m`, `h`

### Options

| Flag | Default | Description |
| ---- | ------- | ----------- |
| `-q` | `false` | Don't show timer on the screen. |


## Install

### Binary release

See [Releases](https://gitlab.com/fj68/go-after/-/releases).

### From source code

```sh
go get gitlab.com/fj68/go-after
cd go-after
go build -o after
```

## Contribution

TBA

### License

MIT License
