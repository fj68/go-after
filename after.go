package main

import (
	"flag"
	"fmt"
	"github.com/gdamore/tcell/v2"
	"os"
	"time"
)

const help = `
after - Simple cli timer

Usage: after [options] <duration>

This command blocks until <duration> passes and return 0.
Press Ctrl-C to quit and it will return 1.

<duration> is a possibly signed sequence of decimal numbers,
each with optional fraction and a unit suffix such as:
	"300ms", "2h45m" or "1h10m10s"

Valid time units are:
	"ms", "s", "m", "h"

Options:
	-q		Don't show timer on the screen. (default: false)
`

func drawText(s tcell.Screen, x, y int, style tcell.Style, text string) {
	row := y
	col := x
	for _, r := range []rune(text) {
		s.SetContent(col, row, r, nil, style)
		col++
	}
}

func HMS(d time.Duration) (int, int, int) {
	h := d.Hours()
	d = d - (time.Duration)(h)*time.Hour
	m := d.Minutes()
	d = d - (time.Duration)(m)*time.Minute
	s := d.Seconds()
	return int(h), int(m), int(s)
}

func main() {
	isQuiet := flag.Bool("q", false, "Don't show timer on the screen.")
	flag.Parse()

	if flag.NArg() == 0 {
		fmt.Print(help)
		os.Exit(0)
	}

	duration, err := time.ParseDuration(flag.Arg(0))
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	var s tcell.Screen
	if !*isQuiet {
		s, err = tcell.NewScreen()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		if err := s.Init(); err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
	}

	start := time.Now()

	for {
		elapsed := time.Since(start)
		if elapsed >= duration {
			if s != nil {
				s.Clear()
				s.Fini()
			}
			os.Exit(0)
		}

		if s != nil {
			s.Clear()
			hour, min, sec := HMS(elapsed)
			drawText(s, 0, 0, tcell.StyleDefault, fmt.Sprintf("%dh%dm%ds", hour, min, sec))
			s.Show()

			for {
				if !s.HasPendingEvent() {
					break
				}
				ev := s.PollEvent()
				switch ev.(type) {
				case *tcell.EventResize:
					s.Sync()
				}
			}
		}
		time.Sleep(500)
	}
}
